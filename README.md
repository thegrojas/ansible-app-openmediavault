<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
    <br>
    <img width="70%"src="./omv-logo.png" alt="OpenMediaVault logo">
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Inactive-inactive.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# OpenMediaVault

This role installs OpenMediaVault on a Debian based system.

It performs the following tasks:

- Adds OMV repository
- Installs OMV
- Installs OMV-Extras Plugin

## 🦺 Requirements

*None*

## 🗃️ Role Variables

You can choose the version of OMV to install by changing the associated variable.

**Example:**

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
    # Choose the version to be installed
    omv_version: arrakis
    omv_extras_version: 4
```

## 📦 Dependencies

*None*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
